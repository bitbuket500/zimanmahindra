package com.say.mahindrafinance.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.base.MahindraFinanceApplication;
import com.say.mahindrafinance.helperClasses.AndroidUtils;
import com.say.mahindrafinance.helperClasses.AppConstants;
import com.say.mahindrafinance.helperClasses.AppWaitDialog;
import com.say.mahindrafinance.helperClasses.InternetConnection;
import com.say.mahindrafinance.helperClasses.SessionManager;
import com.say.mahindrafinance.interfaces.AppDataUrls;
import com.say.mahindrafinance.pojo.get_notification.NotificationListResponse;
import com.say.mahindrafinance.pojo.get_notification.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class NotificationListActivity extends AppCompatActivity {

    RecyclerView recyclerViewNotification;
    private AppWaitDialog mWaitDialog = null;
    SessionManager sessionManager;
    ArrayList<Result> notificationArrayList = new ArrayList<>();
    CustomAdapter customAdapter;
    TextView textViewNoNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
            setActionBarTitleText();
        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        textViewNoNotifications = findViewById(R.id.textViewNoNotifications);
        recyclerViewNotification = findViewById(R.id.recyclerViewNotification);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotification.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        customAdapter = new CustomAdapter();
        recyclerViewNotification.setAdapter(customAdapter); // set the Adapter to RecyclerView

        getNotificationList();
    }

    private void getNotificationList() {

        AndroidUtils.hideKeyboard(NotificationListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getNotifications(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getNotifications Res :", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        NotificationListResponse notificationListResponse = new Gson().fromJson(response, NotificationListResponse.class);
                        if (notificationListResponse.status.equals(AppConstants.SUCCESS)) {
                            textViewNoNotifications.setVisibility(GONE);
                            recyclerViewNotification.setVisibility(View.VISIBLE);
                            ArrayList<Result> result = notificationListResponse.result;
                            notificationArrayList.clear();
                            notificationArrayList.addAll(result);
                            customAdapter.notifyDataSetChanged();
                        } else {
                            // hide recyclerViewNotification and show no contact found message.
                            textViewNoNotifications.setVisibility(View.VISIBLE);
                            recyclerViewNotification.setVisibility(GONE);

                            String message = notificationListResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(NotificationListActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(NotificationListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(NotificationListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void setActionBarTitleText() {
        ActionBar ab = getSupportActionBar();
        LayoutInflater inflater = getLayoutInflater();
        View customLayout = inflater.inflate(R.layout.abs_layout_notifications, null);

        ImageView imageViewBackArrow = customLayout.findViewById(R.id.imageViewBackArrow);
        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationListActivity.super.onBackPressed();
            }
        });

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(customLayout);
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull CustomAdapter.MyViewHolder holder, int position) {

            if (!notificationArrayList.isEmpty()) {
                holder.textViewNotificationTitle.setText(notificationArrayList.get(position).title);
                holder.textViewNotificationDesc.setText(notificationArrayList.get(position).message);
                holder.textViewNotificationDate.setText(notificationArrayList.get(position).createdAt);
            }
        }


        @Override
        public int getItemCount() {
            return notificationArrayList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewNotificationTitle;// init the item view's
            TextView textViewNotificationDesc;// init the item view's
            TextView textViewNotificationDate;// init the item view's

            MyViewHolder(View itemView) {
                super(itemView);

                textViewNotificationTitle = (TextView) itemView.findViewById(R.id.textViewNotificationTitle);
                textViewNotificationDesc = (TextView) itemView.findViewById(R.id.textViewNotificationDesc);
                textViewNotificationDate = (TextView) itemView.findViewById(R.id.textViewNotificationDate);
            }
        }
    }

}
