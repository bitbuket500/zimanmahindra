package com.say.mahindrafinance.activities;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.say.mahindrafinance.R;
import com.say.mahindrafinance.pojo.ContactVO;

import java.util.Objects;

public class AllowAccessibilityServiceActivity extends AppCompatActivity {


    public boolean checkAccessibilityPermission() {
        int accessEnabled = 0;
        try {
            accessEnabled = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if (accessEnabled == 0) {
            // if not construct intent to request permission
            /*Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // request permission via start activity for result
            startActivity(intent);*/

            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allow_accessibility_service);

        //ActivityCompat.requestPermissions(AllowMyLocationActivity.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_BACKGROUND_LOCATION_PERMISSION);


        Button buttonAllowMyLocation = findViewById(R.id.buttonAllowMyLocation);
        buttonAllowMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AllowAccessibilityServiceActivity.this);
                builder.setTitle("Accessibility Features Disclosure");
                builder.setMessage("Our app utilizes Accessibility features to improve usability and provide enhanced functionality for all users. These features may require access to certain device permissions and data. By enabling Accessibility features, you can benefit from improved navigation, enhanced readability, and more. Your privacy and security are important to us, and we assure you that any data collected will be handled in accordance with our privacy policy.");

                builder.setPositiveButton("Enable Accessibility Features", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Open Accessibility settings
                        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        AllowAccessibilityServiceActivity.this.startActivityForResult(intent, 1234);
                    }
                });

                builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Dismiss the dialog
                        dialog.dismiss();
                    }
                });

                builder.setNeutralButton("Learn More", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Open a web page for more information
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mahindrafinance.zicomsaas.com/privacy&policy.html"));
                        AllowAccessibilityServiceActivity.this.startActivity(intent);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }


    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case 1234:
                startAllowMyContactsIntent();
                break;

        }
    }

    private void startAllowMyContactsIntent() {
        Intent intent = new Intent(AllowAccessibilityServiceActivity.this, AllowMyLocationActivity.class);
        startActivity(intent);
        finish();
    }

}
