package com.say.mahindrafinance.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.helperClasses.SessionManager;

import java.util.Objects;

import static android.view.View.GONE;

public class SetUpPinActivity extends AppCompatActivity {

    PinView firstPinView;
    PinView secondPinView;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_pin);

        sessionManager = new SessionManager(this);
        firstPinView = (PinView) findViewById(R.id.firstPinView);
        secondPinView = (PinView) findViewById(R.id.secondPinView);

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstPin = firstPinView.getText().toString();
                String secondPin = secondPinView.getText().toString();

                if (TextUtils.isEmpty(firstPin)){
                    firstPinView.setError("Please enter mPIN");
                    firstPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(secondPin)){
                    secondPinView.setError("Please enter confirm mPIN");
                    secondPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(firstPin) && firstPin.length() != 4){
                    firstPinView.setError("Please enter 4 digit mPIN");
                    firstPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(secondPin) && secondPin.length() != 4 ){
                    secondPinView.setError("Please enter 4 digit confirm mPIN");
                    secondPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(firstPin) && !TextUtils.isEmpty(secondPin) && firstPin.equals(secondPin)){


                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(14);
                    textViewTitle.setText("You have successfully created mPIN.");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(SetUpPinActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(false);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sessionManager.savePin(firstPin);
                            sessionManager.createLoginSession(true);
                            sessionManager.setIsFromLogIn(true);
                            alert.dismiss();
                            Intent intent = new Intent(SetUpPinActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    secondPinView.setError("mPIN and Confirm mPIN must be same.");
                    secondPinView.requestFocus();
                }
            }
        });
    }
}
