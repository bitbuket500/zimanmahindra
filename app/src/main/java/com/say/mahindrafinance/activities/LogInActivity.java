package com.say.mahindrafinance.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.base.MahindraFinanceApplication;
import com.say.mahindrafinance.fcm.UpdateHelper;
import com.say.mahindrafinance.helperClasses.AndroidUtils;
import com.say.mahindrafinance.helperClasses.AppConstants;
import com.say.mahindrafinance.helperClasses.AppWaitDialog;
import com.say.mahindrafinance.helperClasses.InternetConnection;
import com.say.mahindrafinance.helperClasses.SessionManager;
import com.say.mahindrafinance.interfaces.AppDataUrls;
import com.say.mahindrafinance.pojo.login.LogInResponse;
import com.say.mahindrafinance.pojo.send_otp.Result;
import com.say.mahindrafinance.pojo.send_otp.SendOTPResponse;
import com.say.mahindrafinance.pojo.verify_otp.VerifyOTPResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class LogInActivity extends AppCompatActivity implements UpdateHelper.OnUpdateCheckListener {

    Button buttonNext;
    Button buttonProceed;
    Button buttonSubmit;
    TextView textViewResendOTP;
    TextView textViewResendOTPTimer;
    EditText editTextUserMobileNo, editTextUserSAPIDNew;
    SessionManager sessionManager;
    AlertDialog b;
    RelativeLayout rl_mobile_number;
    RelativeLayout rl_otp;
    String otpRefNo;
    String userId;
    PinView pinViewOTP;
    String userMobileNo;
    //    RelativeLayout rl_sap_id;
    private AppWaitDialog mWaitDialog = null;
    String currentVersion = "", onlineVersion = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token = " + token);
        if (!TextUtils.isEmpty(token)) {
            sessionManager.saveFCMToken(token);
        }

        //SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        // Store values.


        editTextUserMobileNo = findViewById(R.id.editTextUserMobileNo);
        editTextUserSAPIDNew = findViewById(R.id.editTextUserSAPIDNew);

        textViewResendOTPTimer = findViewById(R.id.textViewResendOTPTimer);
        textViewResendOTP = findViewById(R.id.textViewResendOTP);
        textViewResendOTP.setEnabled(false);
        textViewResendOTP.setTextColor(getResources().getColor(R.color.light_grey));
        textViewResendOTP.setClickable(false);
        textViewResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP(userMobileNo, "2");
            }
        });

        rl_mobile_number = findViewById(R.id.rl_mobile_number);
        rl_otp = findViewById(R.id.rl_otp);
//        rl_sap_id = findViewById(R.id.rl_sap_id);


        buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLogInData();
            }
        });

        pinViewOTP = findViewById(R.id.pinViewOTP);

        buttonProceed = findViewById(R.id.buttonProceed);
        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = pinViewOTP.getText().toString();

                if (otp.isEmpty()) {
                    pinViewOTP.setError(getResources().getString(R.string.error_please_enter_verification));
                    pinViewOTP.requestFocus();
                    return;
                }

                if (otp.length() != 4) {
                    pinViewOTP.setError(getResources().getString(R.string.error_invalid));
                    pinViewOTP.requestFocus();
                    return;
                }


                verifyOTP(otp);
            }
        });

        UpdateHelper.with(LogInActivity.this)
                .onUpdateCheck(LogInActivity.this)
                .check();

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

       //new GetVersionCode().execute();

    }

    public void validateLogInData() {
        String userSAPIDNew = editTextUserSAPIDNew.getText().toString();
        userMobileNo = editTextUserMobileNo.getText().toString();
        System.out.println("userMobileNo length = " + userMobileNo.length());
        if (InternetConnection.checkConnection(this)) {
            if (TextUtils.isEmpty(userMobileNo)) {
                editTextUserMobileNo.setError("Please enter mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userMobileNo) && userMobileNo.length() != 10) {
                editTextUserMobileNo.setError("Please enter valid mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(userSAPIDNew)) {
                editTextUserSAPIDNew.setError("Please enter SAP Id");
                editTextUserSAPIDNew.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userSAPIDNew) && userSAPIDNew.length() != 8) {
                editTextUserSAPIDNew.setError("Please enter valid SAP ID");
                editTextUserSAPIDNew.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userMobileNo) && !TextUtils.isEmpty(userSAPIDNew)) {
                checkLoginSuccess(userMobileNo, userSAPIDNew);
            }
        } else {
            internetNotAvailableDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

    }

    @Override
    public void onUpdateCheckListener(String urlApp) {
        System.out.println("LoginActivity URLApp: " + urlApp);
        updateVersionPopup();
    }

    private void updateVersionPopup(){
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(GONE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        textViewTitle.setText(" Update info !");
        TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
        textViewMsg.setVisibility(View.VISIBLE);
        textViewMsg.setTextSize(14);
        textViewMsg.setText("New version is Available. Please update application");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("OK");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setVisibility(GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                                //onlineVersionCode = sibElemet.hashCode();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }

        @Override
        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(16);
                    textViewTitle.setText(" Update info !");
                    TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
                    textViewMsg.setVisibility(View.VISIBLE);
                    textViewMsg.setTextSize(14);
                    textViewMsg.setText("New version is Available. Please update application");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

    private void checkLoginSuccess(final String userMobileNo, final String userSAPIDNew) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Login URl = " +  AppDataUrls.postLogin());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postLogin(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("LogIn Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        LogInResponse logInResponse = new Gson().fromJson(response, LogInResponse.class);
                        if (logInResponse.status.equals(AppConstants.SUCCESS)) {
                            com.say.mahindrafinance.pojo.login.Result result = logInResponse.result;
                            if (result != null) {
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName.concat(" ").concat(result.lastName));
                                sessionManager.saveSAPCode(result.sapCode);
                                sessionManager.saveMobileNo(result.mobileNo);
                                if (result.locationInfo != null) {
                                    if (!TextUtils.isEmpty(result.locationInfo.branchName)) {
                                        sessionManager.saveBranchName(result.locationInfo.branchName);
                                    }
                                }
                                sessionManager.saveAuthKey(result.authKey);
                                sendOTP(userMobileNo, "1");
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, logInResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    checkLoginSuccess(userMobileNo, userSAPIDNew);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
                params.put(AppConstants.SAP_ID, userSAPIDNew);
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());

                System.out.println("LogIn Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void sendOTP(final String userMobileNo, final String purpose) {
        rl_mobile_number.setVisibility(View.GONE);
        rl_otp.setVisibility(View.VISIBLE);
        // rl_sap_id.setVisibility(View.GONE);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                long l = millisUntilFinished / 1000;
                if (l > 9) {
                    textViewResendOTPTimer.setText("(00:" + l + ")");
                } else {
                    textViewResendOTPTimer.setText("(00:0" + l + ")");
                }
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                textViewResendOTPTimer.setText("");
                textViewResendOTP.setTextColor(getResources().getColor(R.color.grey));
                textViewResendOTP.setEnabled(true);
                textViewResendOTP.setClickable(true);
            }

        }.start();

        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Login URl = " +  AppDataUrls.postSentOTP());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSentOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("SendOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        SendOTPResponse sendOTPResponse = new Gson().fromJson(response, SendOTPResponse.class);
                        if (sendOTPResponse.status.equals(AppConstants.SUCCESS)) {
                            Result result = sendOTPResponse.result;
                            if (result != null) {
                                otpRefNo = result.otpRefNo;
                                userId = result.userId;

                                rl_mobile_number.setVisibility(View.GONE);
                                rl_otp.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, sendOTPResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    sendOTP(userMobileNo, purpose);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
                params.put(AppConstants.PURPOSE, purpose);
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("SendOTP Params: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void verifyOTP(final String otp) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVerifyOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VerifyOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VerifyOTPResponse verifyOTPResponse = new Gson().fromJson(response, VerifyOTPResponse.class);
                        if (verifyOTPResponse.status.equals(AppConstants.SUCCESS)) {
                            Intent intent = new Intent(LogInActivity.this, TnCnPolicyActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LogInActivity.this, verifyOTPResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    verifyOTP(otp);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.USER_ID, userId);
                params.put(AppConstants.OTP_REF_NO, otpRefNo);
                params.put(AppConstants.OTP, otp);

                System.out.println("VerifyOTP Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

}
