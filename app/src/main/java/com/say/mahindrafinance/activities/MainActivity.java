package com.say.mahindrafinance.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.jackandphantom.circularprogressbar.CircleProgressbar;
import com.say.mahindrafinance.LockService;

import com.say.mahindrafinance.R;
import com.say.mahindrafinance.base.MahindraFinanceApplication;
import com.say.mahindrafinance.bgservice.CameraService;
import com.say.mahindrafinance.bgservice.Config;
//import com.say.mahindrafinance.haptik.Utils;
import com.say.mahindrafinance.fcm.UpdateHelper;
import com.say.mahindrafinance.helperClasses.AndroidUtils;
import com.say.mahindrafinance.helperClasses.AppConstants;
import com.say.mahindrafinance.helperClasses.AppWaitDialog;
import com.say.mahindrafinance.helperClasses.GPSTracker;
import com.say.mahindrafinance.helperClasses.HeartBeatView;
import com.say.mahindrafinance.helperClasses.InternetConnection;
import com.say.mahindrafinance.helperClasses.JsonCacheHelper;
import com.say.mahindrafinance.helperClasses.SessionManager;
import com.say.mahindrafinance.interfaces.AppDataUrls;
import com.say.mahindrafinance.pojo.ContactInfo;
import com.say.mahindrafinance.pojo.emergency_contacts.EmergencyContactsResponse;
import com.say.mahindrafinance.pojo.emergency_contacts.Result;
import com.say.mahindrafinance.pojo.get_tracking.GetTrackingResponse;
import com.say.mahindrafinance.pojo.get_tracking.TrackeeList;
import com.say.mahindrafinance.pojo.get_tracking.TriggerList;
import com.say.mahindrafinance.pojo.set_tracking.SetTrackingResponse;
import com.say.mahindrafinance.tracking.PreTriggerBroadcastReceiver;
import com.say.mahindrafinance.tracking.PreTriggerTrackerService;
import com.say.mahindrafinance.tracking.TrackerService;
import com.wooplr.spotlight.SpotlightView;
import com.wooplr.spotlight.utils.SpotlightListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.view.View.GONE;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, UpdateHelper.OnUpdateCheckListener {

    public final static int REQUEST_CODE = 10101;
    private static final int GALLERY = 1;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 99;
    private static final String TAG = "ClientActivity";
    public static TextView textViewSeconds;
    public static HeartBeatView heartbeat;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    int secondsLeft = 0;
    ContactInfo contactInfo;
    NavigationView navigationView;
    SessionManager sessionManager;
    RelativeLayout rlSOSMap, rlEmployeeCop, rlTracking;
    double latitude;
    double longitude;
    ContactAdapter emergencyContactsAdapter;
    TextView textViewNoEmergencyContacts;
    ArrayList<Result> emergencyContacts = new ArrayList<>();
    int panicId = 0;
    DrawerLayout drawer;
    ImageView userProfileImageView;
    AlertDialog alertDialog;
    CountDownTimer activeTimer;
    Dialog alertDialogPIN;
    boolean isServiceStopped = false;
    int totalUnreadMessages;
    ImageView imageViewChatbot;
    ImageView imageViewToggle;
    ActionBarDrawerToggle toggle;
    LockService lockService;
    PreTriggerTrackerService preTriggerTrackerService;
    View menuAdd;
    View menuNotification;
    private RecyclerView recyclerView;
    private LinearLayout dotsLayout;

    //////////////////////////////////////////////////
    private TextView[] dots;
    private CircleProgressbar circularProgressOuter;
    //    private CircleProgressbar circularProgressInner;
    private boolean isClickable = true;
    private AppWaitDialog mWaitDialog = null;
    private String imageFilePath;
    private String base64Image = "";
    private String preTrackingShareTime = "60";
    String currentVersion = "", onlineVersion = "";
    String Action;
    private static final int PERMISSION_REQUEST_CODE1 = 202;
    //Context context;
    private static Application context1;


    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri, new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static void logStatusToStorage(String userId, String errorLog) {
        try {
            //File pictureFileDir = new File(Environment.getExternalStorageDirectory(), Config.PANIC_FOLDER_NAME);
            //File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), null);
            //File pictureFileDir = new File(Config.PANIC_FOLDER_PATH, Config.PANIC_FOLDER_NAME);
            File pictureFileDir = new File(context1.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
            //File pictureFileDir = new File(context1.getExternalFilesDir(Config.PANIC_FOLDER_NAME), "say_logs.txt");
            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                pictureFileDir.mkdir();
            }

            /*File pictureFileDir = new File(getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                pictureFileDir.mkdir();
            }*/

            /*File path = new File(pictureFileDir, "say_logs.txt");
            if (!path.exists()) {
                path.createNewFile();
            }*/

            String mFileName = pictureFileDir + "say_logs.txt";

            FileWriter logFile = new FileWriter(mFileName, true);
            //FileWriter logFile = new FileWriter(pictureFileDir.getAbsolutePath(), true);
            logFile.append(errorLog + "\n------------------\n");
            logFile.close();
        } catch (Exception e) {
            Log.e(TAG, "Log file error", e);
        }

    }

    public boolean checkAccessibilityPermission() {
        int accessEnabled = 0;
        try {
            accessEnabled = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if (accessEnabled == 0) {
            // if not construct intent to request permission
            /*Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // request permission via start activity for result
            startActivity(intent);*/
            AccessibilityDisclosureDialog.show(this);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!checkAccessibilityPermission()) {
          //  Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
        Action = "";
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imageViewToggle = (ImageView) findViewById(R.id.imageViewToggle);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token = " + token);
        if (!TextUtils.isEmpty(token)) {
            sessionManager.saveFCMToken(token);
        }

        context1 = getApplication();

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        drawer.setBackgroundColor(getResources().getColor(R.color.white));

        textViewNoEmergencyContacts = findViewById(R.id.textViewNoEmergencyContacts);
        textViewSeconds = findViewById(R.id.textViewSeconds);
        recyclerView = findViewById(R.id.recycler);

        //ArrayList<ContactInfo> imageModelArrayList = emergencyContacts();
        emergencyContactsAdapter = new ContactAdapter(this, emergencyContacts);
        recyclerView.setAdapter(emergencyContactsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

        circularProgressOuter = findViewById(R.id.circularProgressOuter);

        imageViewChatbot = findViewById(R.id.imageViewChatbot);
        imageViewChatbot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*initHaptik();
                launchInbox();*/
                Intent intent = new Intent(MainActivity.this, ZimanBotActivity.class);
                //intent.putExtra("From", "Tracking");
                startActivity(intent);
            }
        });

        heartbeat = findViewById(R.id.heartbeat);
        heartbeat.setDurationBasedOnBPM(50);
        heartbeat.start();

        heartbeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClickable && !heartbeat.isHeartBeating()) {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_pin_dialog, null);

                    final PinView pinView = alertLayout.findViewById(R.id.pinView);
                    Button btnSubmit = alertLayout.findViewById(R.id.btnSubmit);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(false);
                    alertDialogPIN = builder.create();
                    pinView.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            if (s.length() == 4) {
                                String pin = sessionManager.getPin();
                                String enteredPin = pinView.getText().toString();

                                if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                    isServiceStopped = true;

                                    circularProgressOuter.setProgress(0);
                                } else {
                                    isServiceStopped = false;
                                    pinView.setError("Please enter valid mPIN");
                                    pinView.requestFocus();
                                }
                            }
                        }
                    });

                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String pin = sessionManager.getPin();
                            String enteredPin = pinView.getText().toString();

                            if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                isServiceStopped = true;
                            } else {
                                isServiceStopped = false;
                                pinView.setError("Please enter valid mPIN");
                                pinView.requestFocus();
                            }
                        }
                    });

                    alertDialogPIN.show();
                }
            }
        });


        heartbeat.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                System.out.println("isClickable onLongClick = " + isClickable);
                System.out.println("isServiceStopped onLongClick = " + isServiceStopped);
                if (isClickable && heartbeat.isHeartBeating()) {
                    isClickable = false;
                    heartbeat.stop();
                    activeTimer = new CountDownTimer(10000, 1000) {
                        public void onTick(long ms) {
                            if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                                secondsLeft = Math.round((float) ms / 1000.0f);
//                                System.out.println("secondsLeft = " + secondsLeft);
                                int pro = 100 - (secondsLeft * 10);
                                circularProgressOuter.setProgressWithAnimation(pro, 0); // Default duration = 1500ms

                                textViewSeconds.setText(secondsLeft + "");
                                textViewSeconds.setVisibility(View.VISIBLE);
//                                System.out.println("long = " + ms);

                                if (isServiceStopped) {
                                    activeTimer.cancel();
                                    circularProgressOuter.setProgressWithAnimation(0, 0);
                                    if (heartbeat != null) {
                                        textViewSeconds.setText("");
                                        textViewSeconds.setVisibility(GONE);
                                        heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_panic_button_yellow));
                                        heartbeat.start();
                                    }

                                    if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                        alertDialogPIN.dismiss();
                                    }

                                    isClickable = true;
                                    isServiceStopped = false;
                                }

                                if (ms < 1999) {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            textViewSeconds.setText("1");
                                            circularProgressOuter.setProgressWithAnimation(90, 0); // Default duration = 1500ms
                                            handler.removeCallbacks(this);
                                        }
                                    }, 1000);
                                }
                            }
                        }

                        public void onFinish() {
                            circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                            textViewSeconds.setText("0");

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                        alertDialogPIN.dismiss();
                                    }

                                    //write your code here to be executed after 1 second
                                    textViewSeconds.setText("ACTIVE");
                                    textViewSeconds.setTextColor(getResources().getColor(R.color.darkRed));
//                                    textViewSeconds.setVisibility(GONE);
                                    circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms

                                    circularProgressOuter.setProgressWithAnimation(0, 0); // Default duration = 1500ms

                                    heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_panic_button_red));
//                                    heartbeat.start();

                                    if (isServiceStopped) {
                                        activeTimer.cancel();
                                        if (heartbeat != null) {
                                            textViewSeconds.setText("");
                                            textViewSeconds.setVisibility(GONE);
                                            heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_panic_button_yellow));
                                            heartbeat.start();
                                        }

                                    } else {
                                        createPanicRequest();
                                    }
//                                    openCamera();
                                    isClickable = true;
                                    isServiceStopped = false;

                                    handler.removeCallbacks(this);
                                }
                            }, 1000);
                        }
                    };
                    activeTimer.start();

                }
                return true;
            }
        });

        rlTracking = findViewById(R.id.rlTracking);
        rlTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showComingSoonDialog();
                Intent intent = new Intent(MainActivity.this, TrackingActivity.class);
                startActivity(intent);
            }
        });

        rlSOSMap = findViewById(R.id.rlSOSMap);
        rlSOSMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EmergencyMapsActivity.class);
                //Intent intent = new Intent(MainActivity.this, EmergencyMapsActivity1.class);
                startActivity(intent);
            }
        });

        rlEmployeeCop = findViewById(R.id.rlEmployeeCop);
        rlEmployeeCop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, VideoCaptureActivity.class);
                startActivity(intent);

            }
        });

        if (InternetConnection.checkConnection(this)) {
            getTrackingApiCall();
            getEmergencyContacts();
        } else {
            internetNotAvailableDialog();
        }

        setUpNavigation();

        GPSTracker gpsTracker = new GPSTracker(this);
        // check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            Location location = gpsTracker.getLocation();
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                System.out.println("latitude = " + latitude);
                System.out.println("longitude = " + longitude);

                sessionManager.saveUserLatitude(String.valueOf(latitude));
                sessionManager.saveUserLongitude(String.valueOf(longitude));
            }
        }

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                Action = "start";
                startSpotLight();
            }
        }, 1000);


//        isMyServiceRunning(TrackerService.class);

        //Add to Activity
//        FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
        //Add to Activity
//        FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications");


//        Intent startIntent = new Intent(getApplicationContext(), MyService.class);
//        startIntent.setAction("ACTION_START_SERVICE");
//        startService(startIntent);

//        Intent ll24 = new Intent(MainActivity.this, AlarmReceiverLifeLog.class);
//        PendingIntent recurringLl24 = PendingIntent.getBroadcast(MainActivity.this, 0, ll24, PendingIntent.FLAG_CANCEL_CURRENT);
//        AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
////        alarms.setRepeating(AlarmManager.RTC_WAKEUP, first_log.getTime(), AlarmManager.INTERVAL_HOUR, recurringLl24); // Log repetition
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, 10000, 60000, recurringLl24); // Log repetition

        /*if (Build.MANUFACTURER.equals("OPPO")) {
            System.out.println("MANUFACTURER = " + Build.MANUFACTURER);
            try {
                Intent intent = new Intent();
                intent.setClassName("com.coloros.safecenter",
                        "com.coloros.safecenter.permission.startup.StartupAppListActivity");
                startActivity(intent);
            } catch (Exception e) {
                try {
                    Intent intent = new Intent();
                    intent.setClassName("com.oppo.safe",
                            "com.oppo.safe.permission.startup.StartupAppListActivity");
                    startActivity(intent);

                } catch (Exception ex) {
                    try {
                        Intent intent = new Intent();
                        intent.setClassName("com.coloros.safecenter",
                                "com.coloros.safecenter.startupapp.StartupAppListActivity");
                        startActivity(intent);
                    } catch (Exception exx) {

                    }
                }
            }
        }

        initOPPO();*/

        //new code by mayur
        /*if (Action.equals("")) {
            if (checkDrawOverlayPermission()) {
                //startService(new Intent(MainActivity.this, PowerButtonService.class));

                powerButtonService = new PowerButtonService();
                Intent mServiceIntent = new Intent(MainActivity.this, powerButtonService.getClass());
                if (!isMyServiceRunning(powerButtonService.getClass())) {
                    Intent myAlarmStart = new Intent(this, LockScreenBroadcastReceiver.class);
                    PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        getApplicationContext().startForegroundService(mServiceIntent);
                        System.out.println("Foreground Service in MainActivity");
                    } else {
                        getApplicationContext().startService(mServiceIntent);
                        System.out.println("Start Service in MainActivity");
                    }
                }
            }
        }*/
        //end


        /*MahindraFinanceApplication.getInstance().setOnVisibilityChangeListener(new MahindraFinanceApplication.ValueChangeListener() {
            @Override
            public void onChanged(Boolean value) {
                Log.d("isAppInBackground", String.valueOf(value));
                isRunning();
            }
        });*/

        //commented by mayur
        lockService = new LockService();
        Intent mServiceIntent = new Intent(MainActivity.this, lockService.getClass());
        if (!isMyServiceRunning(lockService.getClass())) {
            Intent myAlarmStart = new Intent(this, MainActivity.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_IMMUTABLE);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getApplicationContext().startForegroundService(mServiceIntent);
            } else {
                getApplicationContext().startService(mServiceIntent);
            }
        }
        //end

    }

    private void isRunning() {
        preTriggerTrackerService = new PreTriggerTrackerService();
        Intent mServiceIntent = new Intent(MainActivity.this, preTriggerTrackerService.getClass());
        if (isMyServiceRunning(preTriggerTrackerService.getClass())) {
            System.out.println("Service is Running");
            Intent myAlarmStart = new Intent(this, PreTriggerBroadcastReceiver.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(MainActivity.this, 0, myAlarmStart, PendingIntent.FLAG_IMMUTABLE);
            //PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(mServiceIntent);
            } else {
                startService(mServiceIntent);
            }
        } else {
            System.out.println("Service is not Running");
        }
    }

    private boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        MahindraFinanceApplication.getInstance().setOnVisibilityChangeListener(new MahindraFinanceApplication.ValueChangeListener() {
            @Override
            public void onChanged(Boolean value) {
                System.out.println("isAppInBackground:" + String.valueOf(value));
                //Log.d("isAppInBackground", String.valueOf(value));
                isRunning();
            }
        });

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();
    }

    private void initOPPO() {
        System.out.println("MANUFACTURER Init OPPO");

        try {

            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.floatwindow.FloatWindowListActivity"));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            try {

                Intent intent = new Intent("action.coloros.safecenter.FloatWindowListActivity");
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.floatwindow.FloatWindowListActivity"));
                startActivity(intent);
            } catch (Exception ee) {

                ee.printStackTrace();
                try {

                    Intent i = new Intent("com.coloros.safecenter");
                    i.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity"));
                    startActivity(i);
                } catch (Exception e1) {

                    e1.printStackTrace();
                }
            }

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
//        Log.i ("Service status", "Not running");
        return false;
    }

    private void updateTrackingApiCall(final String trackingId) {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Update Tracking Res = ", response);
                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                    mWaitDialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals(AppConstants.SUCCESS)) {
                        // Stop service N Cancel Alarm
                        PreTriggerTrackerService mYourService = new PreTriggerTrackerService();
                        Intent mServiceIntent = new Intent(MainActivity.this, mYourService.getClass());
                        if (isMyServiceRunning(mYourService.getClass())) {
                            stopService(mServiceIntent);
                        }

                        // stop alarm also
                        Intent myAlarm = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
                        PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_IMMUTABLE);
                        //PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                        alarms.cancel(recurringAlarm);


                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                        viewHorizontal.setVisibility(GONE);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setTextSize(14);
                        textViewTitle.setText("SOS Pre-Trigger deactivated successfully.");
                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        btnYes.setText("OK");
                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        btnNo.setVisibility(GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(true);
                        final Dialog alert = builder.create();

                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        alert.show();
                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TRACKING_ID, trackingId.toLowerCase().replace("tracking_id_", ""));
                params.put(AppConstants.STATUS, "0");

                System.out.println("UpdateTracking MA = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setTackingApiCall() {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }
        System.out.println("SetTracking URL: " + AppDataUrls.postSetTracking());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSetTracking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("SetTracking Response: " + response);
                //Log.d("setTracking in Main = ", response);
                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                    mWaitDialog.dismiss();
                }

                SetTrackingResponse setTrackingResponse = new Gson().fromJson(response, SetTrackingResponse.class);
                if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                    int trackingId = setTrackingResponse.result.trackingId;

                    checkAndStartLocationService(trackingId);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                    mWaitDialog.dismiss();
                }
                if (!InternetConnection.checkConnection(MainActivity.this)) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog b = dialogBuilder.create();
                    b.show();
                    dialogBuilder.setCancelable(false);
                    Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                    btnOkay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            b.dismiss();
                        }
                    });
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.TRACKEE_ID, sessionManager.getUserId());
                params.put(AppConstants.TRACKER_ID, "0");
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.SHARE_TIME, preTrackingShareTime);
                params.put(AppConstants.TRACKING_TYPE, "2");

                System.out.println("SetTracking Pre Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * First validation check - ensures that required inputs have been
     * entered, and if so, store them and runs the next check.
     */
    private void checkAndStartLocationService(Integer trackingId) {
        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("pre_trigger_transport_id", "Tracking_id_" + trackingId);
        editor.putString("email", getResources().getString(R.string.fbemail));
        editor.putString("password", getResources().getString(R.string.fbpassword));
        editor.apply();
        // Validate permissions.
        startLocationService();
    }

    private void startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);

                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        }

        preTriggerTrackerService = new PreTriggerTrackerService();
        Intent mServiceIntent = new Intent(MainActivity.this, preTriggerTrackerService.getClass());
        if (!isMyServiceRunning(preTriggerTrackerService.getClass())) {
            Intent myAlarmStart = new Intent(this, PreTriggerBroadcastReceiver.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarmStart, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_MUTABLE);
            //PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(mServiceIntent);
            } else {
                startService(mServiceIntent);
            }
        }


    }

    private void getTrackingApiCall() {
        System.out.println("GetTracking URL =" + AppDataUrls.postGetTracking());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("GetTracking Response =" + response);
                //Log.d("getTracking Response = ", response);

                GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                    List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
                    if (!trackeeList.isEmpty()) {
                        TrackeeList trackerList1 = trackeeList.get(trackeeList.size() - 1);
                        if (trackerList1 != null) {
                            String firebaseKey = trackerList1.firebaseKey;
                            String endTime = trackerList1.endTime;
                            if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                if (!TextUtils.isEmpty(firebaseKey)) {
                                    String trackingIdString = firebaseKey.replace("-", "_");
                                    System.out.println("Tracking trackingIdString = " + trackingIdString);

                                    try {
                                        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date startTimeDate = input.parse(trackerList1.startTime);
                                        Date currentDate = input.parse(trackerList1.currentTime);
//                                                Date currentDate = new Date();

                                        long diff = currentDate.getTime() - startTimeDate.getTime();
                                        long seconds = diff / 1000;
                                        long minutes = seconds / 60;
//                                                long hours = minutes / 60;
//                                                long days = hours / 24;
                                        System.out.println("diff Minutes = " + minutes);

                                        String minDifference = String.valueOf(minutes);
                                        String shareTime = trackerList1.shareTime;

                                        if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTime)) {
                                            int sTime = Integer.parseInt(shareTime);
                                            int diffTime = Integer.parseInt(minDifference);
                                            if (diffTime > sTime) {
                                                // Stop service N Cancel Alarm
                                                TrackerService mYourService = new TrackerService();
                                                Intent mServiceIntent = new Intent(MainActivity.this, mYourService.getClass());
                                                if (isMyServiceRunning(mYourService.getClass())) {
                                                    stopService(mServiceIntent);
                                                }

//                                                        // stop alarm also
//                                                        Intent myAlarm = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
//                                                        PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
//                                                        AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//                                                        alarms.cancel(recurringAlarm);
                                            } else {
                                                //show pop up and decide
                                                LayoutInflater inflater = getLayoutInflater();
                                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                                textViewTitle.setTextSize(16);
                                                textViewTitle.setText("You have already shared location. Do you want to see your shared location?");
                                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                                builder.setView(alertLayout);
                                                builder.setCancelable(false);
                                                final Dialog alert = builder.create();

                                                btnYes.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        sessionManager.setIsFromLogIn(false);
                                                        alert.dismiss();
                                                        Intent intent = new Intent(MainActivity.this, TrackingActivity.class);
                                                        startActivity(intent);
                                                    }
                                                });

                                                btnNo.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        sessionManager.setIsFromLogIn(false);
                                                        alert.dismiss();
                                                    }
                                                });

                                                if (sessionManager.isFromLogIn()) {
                                                    alert.show();
                                                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                }
                                            }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

//                                            startLocationService();
                                }
                            }
                        }
                    }

                    List<TriggerList> triggerList = getTrackingResponse.triggerList;
                    if (!triggerList.isEmpty()) {
                        TriggerList triggerList1 = triggerList.get(triggerList.size() - 1);
                        if (triggerList1 != null) {
                            String firebaseKey = triggerList1.firebaseKey;
                            String endTime = triggerList1.endTime;
                            if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                if (!TextUtils.isEmpty(firebaseKey)) {
                                    String trackingIdString = firebaseKey.replace("-", "_");
                                    System.out.println("PreTrigger trackingIdString = " + trackingIdString);

                                    try {
                                        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date startTimeDate = input.parse(triggerList1.startTime);
                                        Date currentDate = input.parse(triggerList1.currentTime);
                                        long diff = currentDate.getTime() - startTimeDate.getTime();
                                        long seconds = diff / 1000;
                                        long minutes = seconds / 60;

                                        String minDifference = String.valueOf(minutes);
                                        String shareTime = triggerList1.shareTime;
                                        System.out.println("PreTrigger minDifference = " + minDifference);

                                        if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTime)) {
                                            int sTime = Integer.parseInt(shareTime);
                                            int diffTime = Integer.parseInt(minDifference);
                                            if (diffTime > sTime) {
                                                // Stop service N Cancel Alarm
                                                preTriggerTrackerService = new PreTriggerTrackerService();
                                                Intent mPretriggerTrackerServiceIntent = new Intent(MainActivity.this, preTriggerTrackerService.getClass());
                                                if (isMyServiceRunning(preTriggerTrackerService.getClass())) {
                                                    stopService(mPretriggerTrackerServiceIntent);
                                                }

                                                // stop alarm also
                                                Intent myAlarmCancel = new Intent(MainActivity.this, PreTriggerBroadcastReceiver.class);
                                                PendingIntent recurringAlarm2 = PendingIntent.getBroadcast(MainActivity.this, 0, myAlarmCancel, PendingIntent.FLAG_MUTABLE);
                                                //PendingIntent recurringAlarm2 = PendingIntent.getBroadcast(MainActivity.this, 0, myAlarmCancel, PendingIntent.FLAG_CANCEL_CURRENT);
                                                AlarmManager alarms2 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                alarms2.cancel(recurringAlarm2);

                                            } else {
                                                // Update layout and allow to call updateTracking api.
                                                SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                                // Store values.
                                                SharedPreferences.Editor editor = mPrefs.edit();
                                                editor.putString("pre_trigger_transport_id", trackingIdString);
                                                editor.apply();
                                                startLocationService();
                                            }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /////////////////////////////////////////////
    private void showComingSoonDialog() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(GONE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Coming soon");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("OK");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setVisibility(GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
//                finish();
            }
        });
    }

    private void getEmergencyContacts() {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getEmergencyContacts(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getEmergencyContacts", response);
                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                    mWaitDialog.dismiss();
                }
                JsonCacheHelper.writeToJson(MainActivity.this, response, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
                EmergencyContactsResponse emergencyContactsResponse = new Gson().fromJson(response, EmergencyContactsResponse.class);
                if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                    textViewNoEmergencyContacts.setVisibility(GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    ArrayList<Result> result = emergencyContactsResponse.result;
                    emergencyContacts.clear();
                    emergencyContacts.addAll(result);
                    emergencyContactsAdapter.notifyDataSetChanged();
                } else {
                    // hide recyclerview and show no contact found message.
                    textViewNoEmergencyContacts.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(GONE);

                    String message = emergencyContactsResponse.message;
                    if (message.contains("Invalid") || message.contains("invalid")) {

                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                        viewHorizontal.setVisibility(GONE);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setTextSize(16);
                        textViewTitle.setText(message);
                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        btnYes.setText("OK");
                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        btnNo.setVisibility(GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(false);
                        final Dialog alert = builder.create();

                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                                sessionManager.createLoginSession(false);
                                sessionManager.throwOnLogIn();
                            }
                        });

                        alert.show();
                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                    mWaitDialog.dismiss();
                }
                if (!InternetConnection.checkConnection(MainActivity.this)) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog b = dialogBuilder.create();
                    b.show();
                    dialogBuilder.setCancelable(false);
                    Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                    btnOkay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            b.dismiss();
                        }
                    });
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void createPanicRequest() {

        if (InternetConnection.checkConnection(this)) {
            AndroidUtils.hideKeyboard(MainActivity.this);
            if (mWaitDialog != null) {
                mWaitDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("createPanicRequest = ", response);
                    if (mWaitDialog != null && mWaitDialog.isShowing()) {
                        mWaitDialog.dismiss();
                    }

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {

                            int result = jsonObject.getInt("result");
                            panicId = result;

                            openCameraService();

                        } else {
                            String message = jsonObject.getString("message");
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (mWaitDialog != null && mWaitDialog.isShowing()) {
                        mWaitDialog.dismiss();
                    }
                    if (!InternetConnection.checkConnection(MainActivity.this)) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog b = dialogBuilder.create();
                        b.show();
                        dialogBuilder.setCancelable(false);
                        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                        btnOkay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                b.dismiss();
                            }
                        });
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.USER_ID, sessionManager.getUserId());
                    params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                    params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                    params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                    params.put(AppConstants.USER_LONG, String.valueOf(longitude));

                    System.out.println("Create Panic Params = " + params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

        } else {
            internetNotAvailableDialog();
        }
    }

    private void startSpotLight() {
        try {
            new SpotlightView.Builder(MainActivity.this).introAnimationDuration(500).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("SOS").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("FEAR SHOULDN'T SHUT YOU DOWN. IT SHOULD WAKE YOU UP!Find yourself in an emergency or in distress,  Simply click this button to activate an SOS ALERT").maskColor(Color.parseColor("#dc000000")).target(circularProgressOuter).enableRevealAnimation(false).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("1").setListener(new SpotlightListener() {
                @Override
                public void onUserClicked(String s) {
                    new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("EMERGENCY MAPS").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("THE SECRET OF ULTIMATE SAFETY IS TO NEVER PANIC.Easy access to nearby emergency services like hospitals, police stations, fire stations… along with Mahindra Finance Branch & Contact number near you.").maskColor(Color.parseColor("#dc000000")).target(rlSOSMap).enableRevealAnimation(false).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("2").setListener(new SpotlightListener() {
                        @Override
                        public void onUserClicked(String s) {
                            new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("EMPLOYEE COP").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("BE THAT CHANGE YOU WANT TO SEE.Capture & Record an unusual incident  and share with Mahindra Finance Safety Team for further investigation. ").maskColor(Color.parseColor("#dc000000")).target(rlEmployeeCop).enableRevealAnimation(false).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("3").setListener(new SpotlightListener() {
                                @Override
                                public void onUserClicked(String s) {
                                    new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("TRACKING").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("OUT OF HOME, BUT NOT OUT OF SIGHT.Efficient Tracking & Geo Fencing to monitor your team mates in real time.").maskColor(Color.parseColor("#dc000000")).target(rlTracking).enableRevealAnimation(false).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("4").setListener(new SpotlightListener() {
                                        @Override
                                        public void onUserClicked(String s) {
                                            new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("EMERGENCY CONTACTS").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("QUICK  ACCESS CONTACTS AT YOUR FINGER TIPS.Stay Connected with your Emergency Contacts.  ZIMAN Safety Officers connect to update your emergency contacts during SOS.").maskColor(Color.parseColor("#dc000000")).target(recyclerView).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("5").setListener(new SpotlightListener() {
                                                @Override
                                                public void onUserClicked(String s) {
                                                    new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("SOS PRE TRIGGER").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("FEEL UNSAFE, LET’S BE PREVENTIVE.ZIMAN Safety Officers will pro actively monitor your geo-location and stay connected with you for 60 minutes or until deactivated. If in danger an Auto SOS Alert is activated.").maskColor(Color.parseColor("#dc000000")).target(menuAdd).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("6").setListener(new SpotlightListener() {
                                                        @Override
                                                        public void onUserClicked(String s) {
                                                            new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("NOTIFICATION").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("STAY UP-TO-DAY WITH SAFETY UPDATES.Stay ahead of the game, with safety alerts and Smart Tips & Trick along with Do’s and Don’ts to keep you safe and alert.").maskColor(Color.parseColor("#dc000000")).target(menuNotification).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("7").setListener(new SpotlightListener() {
                                                                @Override
                                                                public void onUserClicked(String s) {
                                                                    new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("AI BOT").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("YOUR VIRTUAL ‘SAFETY BUDDY’ ON THE GO.You are never alone; Our Live Safety Responsive AI BOT will be there with you throughout when you feel unsafe or if you need more information and knowledge regarding ZIMAN.").maskColor(Color.parseColor("#dc000000")).target(imageViewChatbot).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("8").setListener(new SpotlightListener() {
                                                                        @Override
                                                                        public void onUserClicked(String s) {
                                                                            new SpotlightView.Builder(MainActivity.this).introAnimationDuration(0).performClick(true).fadeinTextDuration(400).headingTvColor(Color.parseColor("#e31837")).headingTvSize(20).headingTvText("MENU PANEL").subHeadingTvColor(Color.parseColor("#ffffff")).subHeadingTvSize(16).subHeadingTvText("A DROP DOWN PANEL WITH ALL THAT YOU MIGHT NEED.Options like: Edit your profile, Add or Change your Emergency Contacts,  Feature Tutorials, Terms & Conditions, More About who we are and our contact details Or Simply Raise A Query").maskColor(Color.parseColor("#dc000000")).target(imageViewToggle).lineAnimDuration(400).lineAndArcColor(Color.parseColor("#e31837")).dismissOnTouch(true).dismissOnBackPress(true).enableDismissAfterShown(false).usageId("9").setListener(new SpotlightListener() {
                                                                                @Override
                                                                                public void onUserClicked(String s) {
                                                                                    imageViewToggle.setVisibility(GONE);
                                                                                }
                                                                            }).show();//UNIQUE ID;
                                                                        }
                                                                    }).show();//UNIQUE ID;
                                                                }
                                                            }).show();//UNIQUE ID;
                                                        }
                                                    }).show();//UNIQUE ID;
                                                }
                                            }).show();//UNIQUE ID;
                                        }
                                    }).show();//UNIQUE ID;
                                }
                            }).show();//UNIQUE ID;
                        }
                    }).show();//UNIQUE ID;
                }
            }).show();//UNIQUE ID;
        } catch (IllegalStateException e) {

        }
    }

    private void setUpNavigation() {
        if (navigationView != null) {
            View hView = navigationView.getHeaderView(0);
            TextView userName = hView.findViewById(R.id.textViewUserName);
            TextView textViewSAPID = hView.findViewById(R.id.textViewSAPID);
            TextView textViewAddress = hView.findViewById(R.id.textViewAddress);
            ImageView imageViewEdit = hView.findViewById(R.id.imageViewEdit);
            imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isStoragePermissionGranted()) {
                        selectImageDialog(v.getContext());
                    }
                }
            });

            userProfileImageView = hView.findViewById(R.id.imageViewUserProfile);
            if (!TextUtils.isEmpty(sessionManager.getUserName())) {
                userName.setText(sessionManager.getUserName());
            }

            if (!TextUtils.isEmpty(sessionManager.getSAPCode())) {
                textViewSAPID.setText(sessionManager.getSAPCode());
            }

            if (!TextUtils.isEmpty(sessionManager.getProfileImage())) {
//                RequestOptions options = new RequestOptions()
//                        .centerCrop()
//                        .placeholder(R.drawable.ic_user_profile)
//                        .error(R.drawable.ic_user_profile);
//
//                Glide.with(this).load(sessionManager.getProfileImage()).apply(options).into(userProfileImageView);

                byte[] decodedString = Base64.decode(sessionManager.getProfileImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                userProfileImageView.setImageBitmap(decodedByte);
            }

            if (!TextUtils.isEmpty(sessionManager.getBranchName())) {
                textViewAddress.setText(sessionManager.getBranchName());
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.v("MainActivity", "Permission is granted");

                return true;
            }

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.v("MainActivity", "Permission is granted");

                return true;
            } else {

                Log.v("MainActivity", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("MainActivity", "Permission is granted");
            return true;
        }
    }

    public void selectImageDialog(Context context) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_select_image, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();

            TextView txtGalley, txtCamera;

            txtGalley = dialogView.findViewById(R.id.txtGalley);
            txtCamera = dialogView.findViewById(R.id.txtCamera);

            txtGalley.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                    openGallery();

                }
            });

            txtCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    openCamera();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */);

        imageFilePath = image.getAbsolutePath();

        return image;
    }

    public void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(MainActivity.this, "com.say.mahindrafinance.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, REQUEST_CAPTURE_IMAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("MainActivity", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }*/

        switch (requestCode) {
            /*if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v("MainActivity", "Permission: " + permissions[0] + "was " + grantResults[0]);
                //resume tasks needing this permission
            }
            break;*/

            case PERMISSION_REQUEST_CODE1:
                if (!checkPermissionBackground()) {
                    System.out.println("PERMISSION_REQUEST_CODE1");
                    popupPermission();
                    //requestBackgroundPermission();
                }
                /*if (grantResults.length > 0) {
                    System.out.println("1 PERMISSION_REQUEST_CODE1");
                    preTriggerPopup();
                    //Toast.makeText(MainActivity.this, "PERMISSION_REQUEST_CODE1 if onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("2 PERMISSION_REQUEST_CODE1");
                    //Toast.makeText(MainActivity.this, "PERMISSION_REQUEST_CODE1 else onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                    preTriggerPopup();
                }*/
                break;
        }


    }

    private void popupPermissionInfo() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.popup_permission1, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);

        String textMsg = "In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        textViewTitle.setText(Html.fromHtml(textMsg));

        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        //btnYes.setText("Close");
        btnNo.setVisibility(View.INVISIBLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();

                if (!checkPermissionBackground()) {
                    System.out.println("MainActivity2");
                    requestBackgroundPermission();
                } else {
                    System.out.println("MainActivity3");
                    preTriggerPopup();
                }

            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == REQUEST_CODE) {
//            if (Settings.canDrawOverlays(this)) {
//                startService(new Intent(this, PowerButtonService.class));
//            }
//        }
        if (requestCode == GALLERY && resultCode != 0) {
            Uri mImageUri = data.getData();
            try {
                Image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                if (getOrientation(getApplicationContext(), mImageUri) != 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(getOrientation(getApplicationContext(), mImageUri));
                    if (rotateImage != null) rotateImage.recycle();
                    rotateImage = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix, true);
                    userProfileImageView.setImageBitmap(rotateImage);
                } else userProfileImageView.setImageBitmap(Image);

                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(mImageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                base64Image = encodeTobase64(yourSelectedImage);
                sessionManager.saveProfileImage(base64Image);


                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                viewHorizontal.setVisibility(GONE);
                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                textViewTitle.setTextSize(14);
                textViewTitle.setText("Profile photo updated successfully.");
                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                btnYes.setText("OK");
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                btnNo.setVisibility(GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(true);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        if (requestCode == REQUEST_CAPTURE_IMAGE) {
            System.out.println("imageFilePath = " + imageFilePath);
            if (!TextUtils.isEmpty(imageFilePath)) {

                File imgFile = new File(imageFilePath);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    System.out.println("If Yes");
                    if (myBitmap != null) {
                        userProfileImageView.setImageBitmap(myBitmap);
                        base64Image = encodeTobase64(myBitmap);

                        sessionManager.saveProfileImage(base64Image);

                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                        viewHorizontal.setVisibility(GONE);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setTextSize(14);
                        textViewTitle.setText("Profile photo updated successfully.");
                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        btnYes.setText("OK");
                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        btnNo.setVisibility(GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(true);
                        final Dialog alert = builder.create();

                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        alert.show();
                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }
                } else {
                    System.out.println("Else Not");
                }
            }
        }

        if (requestCode == PERMISSION_REQUEST_CODE1) {
            System.out.println("MainActivity4");
            //Toast.makeText(MainActivity.this, "PERMISSION_REQUEST_CODE1 onActivityResult", Toast.LENGTH_SHORT).show();
        }

        drawer.closeDrawer(GravityCompat.START);
        //System.out.println("alertDialog = " + alertDialog.isShowing());
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    private void openCameraService() {
        Intent intent = new Intent(getApplicationContext(), CameraService.class);
        intent.putExtra(Config.PING_PANIC_ID, panicId);
        //intent.putExtra(Config.PING_PHOTO, Config.PING_PHOTO);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                menuAdd = findViewById(R.id.menu_add);
                menuNotification = findViewById(R.id.menu_notification);
                // SOME OF YOUR TASK AFTER GETTING VIEW REFERENCE

            }
        });

        // Find the menu item we are interested in.
//        menuAdd = menu.findItem(R.id.menu_add);
//        menuNotification = menu.findItem(R.id.menu_notification);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                //add the function to perform here
                boolean preTriggerOn = sessionManager.isPreTriggerOn();
                if (preTriggerOn) {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_pin_dialog, null);

                    final PinView pinView = alertLayout.findViewById(R.id.pinView);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setText("Please enter mPIN to deactivate SOS Pre-Trigger.");
                    Button btnSubmit = alertLayout.findViewById(R.id.btnSubmit);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    alertDialogPIN = builder.create();
                    pinView.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
//                            System.out.println("pop up string = " + s);
//                            System.out.println("pop up string = " + s.length());

                            if (s.length() == 4) {
                                String pin = sessionManager.getPin();
                                String enteredPin = pinView.getText().toString();

                                if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                    alertDialogPIN.dismiss();
                                    sessionManager.setIsPreTriggerOn(false);
                                    SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                    String pre_trigger_transport_id = mPrefs.getString("pre_trigger_transport_id", "");
                                    updateTrackingApiCall(pre_trigger_transport_id);
                                } else {
                                    pinView.setError("Please enter valid mPIN");
                                    pinView.requestFocus();
                                }
                            }
                        }
                    });

                    alertDialogPIN.show();
                } else {

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                        System.out.println("MainActivity1");
                        preTriggerPopup();
                    } else {

                        if (!checkPermissionBackground()) {
                            popupPermissionInfo();
                        } else {
                            preTriggerPopup();
                        }

                        /*if (!checkPermissionBackground()) {
                            System.out.println("MainActivity2");
                            requestBackgroundPermission();
                        } else {
                            System.out.println("MainActivity3");
                            preTriggerPopup();
                        }*/
                    }


                }

                return (true);
            case R.id.menu_notification:
                //add the function to perform here
                Intent intent = new Intent(MainActivity.this, NotificationListActivity.class);
                startActivity(intent);
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private boolean checkPermissionBackground() {
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_BACKGROUND_LOCATION);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestBackgroundPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_BACKGROUND_LOCATION}, PERMISSION_REQUEST_CODE1);
    }

    private void popupPermission() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.popup_permission, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);

        String textMsg = "In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        textViewTitle.setText(Html.fromHtml(textMsg));

        //textViewTitle.setText("In order to enable background location access, users must set the \"Allow all the time\" option for your app's location permission (Android 11 or higher)");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                preTriggerPopup();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void preTriggerPopup() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        textViewTitle.setText("Are you sure, you want to activate SOS Pre-Trigger?");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();

                sessionManager.setIsPreTriggerOn(true);
                setTackingApiCall();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_emergency_contacts:
                Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_raise_a_query:
                Intent intentRaiseAQuery = new Intent(MainActivity.this, RaiseAQueryActivity.class);
                startActivity(intentRaiseAQuery);
                break;
            case R.id.nav_settings:
                Intent intentChangeMPIN = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intentChangeMPIN);
                break;
            case R.id.nav_about_us:
                Intent intentAboutUs = new Intent(MainActivity.this, AboutUsActivity.class);
                startActivity(intentAboutUs);
                break;
            case R.id.nav_help:
                Intent intentHelp = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intentHelp);
                break;
            case R.id.nav_tutorial:
                Intent intentTutorial = new Intent(MainActivity.this, TutorialActivity.class);
                startActivity(intentTutorial);
                break;
            case R.id.nav_terms_n_condition_privacy:
                Intent intentTNC = new Intent(MainActivity.this, TnCActivity.class);
                startActivity(intentTNC);
                break;

            case R.id.delete:
                AccessibilityDisclosureDialog.showAccoutDelete(this);
                break;
            case R.id.nav_privacy_policy:
                Intent intentPrivacyPolicy = new Intent(MainActivity.this, PrivacyPolicyActivity.class);
                startActivity(intentPrivacyPolicy);
                break;
            case R.id.nav_log_out:

                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(true);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sessionManager.createLoginSession(false);
                        sessionManager.throwOnLogIn();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                break;
        }

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onUpdateCheckListener(String urlApp) {
        System.out.println("MainActivity URLApp: " + urlApp);
        updateVersionPopup();
    }

    private void updateVersionPopup() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(GONE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        textViewTitle.setText(" Update info !");
        TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
        textViewMsg.setVisibility(View.VISIBLE);
        textViewMsg.setTextSize(14);
        textViewMsg.setText("New version is Available. Please update application");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("OK");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setVisibility(GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

        private LayoutInflater inflater;
        private ArrayList<Result> emergencyContacts;

        public ContactAdapter(Context ctx, ArrayList<Result> emergencyContacts) {

            inflater = LayoutInflater.from(ctx);
            this.emergencyContacts = emergencyContacts;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflater.inflate(R.layout.contact_recycler_item, parent, false);
            MyViewHolder holder = new MyViewHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
            if (!TextUtils.isEmpty(emergencyContacts.get(position).userDp)) {

            }

//            holder.ivContactImage.setImageDrawable(getResources().getDrawable());
            holder.tvContactName.setText(emergencyContacts.get(position).userName);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext()).anchorView(holder.itemView)
//                        .text(R.string.string_screening_system)
                            .gravity(Gravity.TOP).dismissOnOutsideTouch(true).dismissOnInsideTouch(false).modal(true).animated(true).contentView(R.layout.tooltip_custom_contact_view).focusable(true).build();

                    ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                    imageViewCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                            Intent i = new Intent(Intent.ACTION_DIAL, u);
                            startActivity(i);
                        }
                    });

                    ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                    imageViewSMS.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                            smsIntent.setType("vnd.android-dir/mms-sms");
                            smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                            smsIntent.putExtra("sms_body", "");
                            startActivity(smsIntent);
                        }
                    });

                    TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                    txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                    TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                    txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                    TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                    txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                    tooltip.show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return emergencyContacts.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvContactName;
            ImageView ivContactImage;

            public MyViewHolder(View itemView) {
                super(itemView);

                tvContactName = itemView.findViewById(R.id.tvContactName);
                ivContactImage = itemView.findViewById(R.id.ivContactImage);
            }

        }
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en").timeout(30000).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").referrer("http://www.google.com").get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                                //onlineVersionCode = sibElemet.hashCode();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(16);
                    textViewTitle.setText(" Update info !");
                    TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
                    textViewMsg.setVisibility(View.VISIBLE);
                    textViewMsg.setTextSize(14);
                    textViewMsg.setText("New version is Available. Please update application");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

}
