package com.say.mahindrafinance.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

public class AccessibilityDisclosureDialog {

    public static void show(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Accessibility Features Disclosure");
        builder.setMessage("Our app utilizes Accessibility features to improve usability and provide enhanced functionality for all users. These features may require access to certain device permissions and data. By enabling Accessibility features, you can benefit from improved navigation, enhanced readability, and more. Your privacy and security are important to us, and we assure you that any data collected will be handled in accordance with our privacy policy.");

        builder.setPositiveButton("Enable Accessibility Features", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Open Accessibility settings
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                context.startActivity(intent);
            }
        });

        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Learn More", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Open a web page for more information
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mahindrafinance.zicomsaas.com/privacy&policy.html"));
                context.startActivity(intent);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public static void showAccoutDelete(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Account Delete Request");
        builder.setMessage("You have the right to request the deletion of your account and associated data from our system. To request data deletion, please contact our support team at zicomsaaas@gmail.com");


        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss the dialog
                dialog.dismiss();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }
}