package com.say.mahindrafinance.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.say.mahindrafinance.R;

public class VideoPlayActivity extends AppCompatActivity{

    private VideoView vv;
    private MediaController mediacontroller;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        progressBar = (ProgressBar) findViewById(R.id.progrss);
        progressBar.setVisibility(View.VISIBLE);
        vv = (VideoView) findViewById(R.id.vv);
        mediacontroller = new MediaController(this);
        mediacontroller.setAnchorView(vv);


//        https://firebasestorage.googleapis.com/v0/b/sayapp-254613.appspot.com/o/tutorials%2FEmergancy%20Maps.mp4?alt=media&token=a77dfa11-9a4b-4e63-a0af-929226d5b563
//        https://firebasestorage.googleapis.com/v0/b/lifesaver-18f28.appspot.com/o/flood.mp4?alt=media&token=179d7e4e-7171-4a87-b1f8-b1fc3d976c60
//        String s = "https://firebasestorage.googleapis.com/v0/b/lifesaver-18f28.appspot.com/o/flood.mp4?alt=media&token=179d7e4e-7171-4a87-b1f8-b1fc3d976c60";
//        vv.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/sayapp-254613.appspot.com/o/Tracking.mp4?alt=media&token=a77dfa11-9a4b-4e63-a0af-929226d5b563"));

      /*  Intent intent = getIntent();
        if (intent != null){
            Uri uri = Uri.parse(intent.getStringExtra("url"));
            System.out.println("uri ff = " + uri);
//            vv.setVideoPath(uri.toString());
            vv.setVideoURI(uri);
//            vv.requestFocus();
//            mediaSource = new ExtractorMediaSource(Uri.parse("https://firebasestorage.googleapis.com/v0/b/lifesaver-18f28.appspot.com/o/flood.mp4?alt=media&token=179d7e4e-7171-4a87-b1f8-b1fc3d976c60"),
//                    dataSourceFactory,
//                    extractorsFactory,
//                    null,
//                    null);
//
//            player = ExoPlayerFactory.newSimpleInstance(this,trackSelector);
//
//            player.addListener(this);
//            player.prepare(mediaSource);
//
//            Log.v("TEST","playing state : " + player.getPlaybackState());

        }*//*

        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                vv.start();
            }
        });*/


        FirebaseStorage storage = FirebaseStorage.getInstance("gs://sayapp-254613.appspot.com/");
        StorageReference storageRef = storage.getReference().child("/tutorials/");
        storageRef.child("/Tracking.mp4").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                vv.setVideoURI(uri);

                vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    // Close the progress bar and play the video
                    public void onPrepared(MediaPlayer mp) {
                        progressBar.setVisibility(View.GONE);
                        vv.start();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

}
