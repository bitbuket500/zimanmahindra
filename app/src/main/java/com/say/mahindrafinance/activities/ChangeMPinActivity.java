package com.say.mahindrafinance.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.helperClasses.SessionManager;

import java.util.Objects;

import static android.view.View.GONE;

public class ChangeMPinActivity extends AppCompatActivity {

    PinView currentPinView;
    PinView newPinView;
    PinView confirmPinView;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sessionManager = new SessionManager(this);
        currentPinView = findViewById(R.id.currentPinView);
        newPinView = findViewById(R.id.newPinView);
        confirmPinView = findViewById(R.id.confirmPinView);

        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentPin = currentPinView.getText().toString();
                final String newPin = newPinView.getText().toString();
                String confirmPin = confirmPinView.getText().toString();
                String mPIN = sessionManager.getPin();
                if (TextUtils.isEmpty(currentPin)) {
                    currentPinView.setError("Please enter current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(newPin)) {
                    newPinView.setError("Please enter new mPIN");
                    newPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(confirmPin)) {
                    confirmPinView.setError("Please enter confirm mPIN");
                    confirmPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(currentPin) && currentPin.length() != 4){
                    currentPinView.setError("Please enter 4 digit current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(newPin) && newPin.length() != 4){
                    newPinView.setError("Please enter 4 digit new mPIN");
                    newPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(confirmPin) && confirmPin.length() != 4 ){
                    confirmPinView.setError("Please enter 4 digit confirm mPIN");
                    confirmPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(currentPin) && !TextUtils.isEmpty(mPIN) && !currentPin.equals(mPIN)) {
                    currentPinView.setError("Please enter valid current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(newPin) && !TextUtils.isEmpty(confirmPin) && newPin.equals(confirmPin)) {

                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(14);
                    textViewTitle.setText("You have successfully changed mPIN.");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangeMPinActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(false);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sessionManager.savePin(newPin);
                            sessionManager.createLoginSession(true);
                            alert.dismiss();
                            finish();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    confirmPinView.setError("New mPIN and Confirm mPIN must be same.");
                    confirmPinView.requestFocus();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
