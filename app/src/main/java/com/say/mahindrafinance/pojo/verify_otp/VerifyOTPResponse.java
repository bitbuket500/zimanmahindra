package com.say.mahindrafinance.pojo.verify_otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.say.mahindrafinance.pojo.send_otp.Result;

import java.io.Serializable;

public class VerifyOTPResponse implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("message")
    @Expose
    public String message;
}
