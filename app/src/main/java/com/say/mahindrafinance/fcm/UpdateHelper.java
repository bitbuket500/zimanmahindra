package com.say.mahindrafinance.fcm;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class UpdateHelper {

    public static String KEY_UPDATE_ENABLE = "is_update";
    public static String KEY_UPDATE_VERSION = "version";
    public static String KEY_UPDATE_URL = "update_url";
    public static String KEY_UPDATE_VERSION_CODE = "version_code";
    int versionCode;
    String currentVersion1, versionCode1;

    public interface OnUpdateCheckListener {
        void onUpdateCheckListener(String urlApp);
    }

    public static Builder with(Context context) {
        return new Builder(context);
    }

    private OnUpdateCheckListener onUpdateCheckListener;
    private Context context;

    public UpdateHelper(Context context, OnUpdateCheckListener onUpdateCheckListener) {
        this.onUpdateCheckListener = onUpdateCheckListener;
        this.context = context;
    }

    public void check() {
        FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        if (remoteConfig.getBoolean(KEY_UPDATE_ENABLE)) {
            String currentVersion = remoteConfig.getString(KEY_UPDATE_VERSION);
            String appVersion = getAppVersion(context);
            String updateURL = remoteConfig.getString(KEY_UPDATE_URL);
            String appVersionCode = remoteConfig.getString(KEY_UPDATE_VERSION_CODE);

            try {
                //onlineVersionCode = Constants.onlineVersion;
                currentVersion1 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
                /*Log.e("Current Version1", "::" + currentVersion1);
                Log.e("Current Version code1", "::" + versionCode);*/
                versionCode1 = String.valueOf(versionCode);
                System.out.println("UpdateHelper Current Version1: " + currentVersion1);
                System.out.println("UpdateHelper Current Version code1: " + versionCode + " & " + versionCode1);

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            System.out.println("UpdateHelper appVersionCode: " + appVersionCode);
            System.out.println("UpdateHelper currentVersion: " + currentVersion);
            System.out.println("UpdateHelper appVersion: " + appVersion);

            if (!TextUtils.equals(appVersionCode, versionCode1) && onUpdateCheckListener != null)
                onUpdateCheckListener.onUpdateCheckListener(updateURL);

            /*if (!TextUtils.equals(currentVersion, appVersion) && onUpdateCheckListener != null)
                onUpdateCheckListener.onUpdateCheckListener(updateURL);*/
        }
    }

    private String getAppVersion(Context context) {
        String result = "";

        try {
            result = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            result = result.replaceAll("[a-zA-Z]|-", "");

            System.out.println("UpdateHelper Result: " + result);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static class Builder {

        private Context context;
        private OnUpdateCheckListener onUpdateCheckListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateCheck(OnUpdateCheckListener onUpdateCheckListener) {
            this.onUpdateCheckListener = onUpdateCheckListener;
            return this;
        }

        public UpdateHelper build() {
            return new UpdateHelper(context, onUpdateCheckListener);
        }

        public UpdateHelper check() {
            UpdateHelper updateHelper = build();
            updateHelper.check();

            return updateHelper;
        }
    }
}
