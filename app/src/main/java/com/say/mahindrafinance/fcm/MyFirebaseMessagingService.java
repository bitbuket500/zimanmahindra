package com.say.mahindrafinance.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.activities.GetTrackingActivity;

import org.json.JSONException;
import org.json.JSONObject;

//import ai.haptik.android.sdk.HaptikLib;

/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private static final String TAG = "FCM Service";
    private static final String ADMIN_CHANNEL_ID = "channel-01";
    private static int count = 0;
    NotificationManager notificationManager;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        //HaptikLib.setFcmDeviceToken(this, refreshedToken);
    }

    /* @Override
     public void onMessageReceived(RemoteMessage remoteMessage) {

         Intent broadcast = new Intent(this, NotificationReceiver.class);
         broadcast.setAction("OPEN_NEW_ACTIVITY");
         sendBroadcast(broadcast);

 //        final Map<String, String> data = remoteMessage.getData();
 //        if (HaptikNotificationManager.isHaptikNotification(data)) {
 //            if (!HaptikLib.isInitialized()) {
 //                Handler mainHandler = new Handler(this.getMainLooper());
 //                mainHandler.post(new Runnable() {
 //                    @Override
 //                    public void run() {
 //                        ai.haptik.android.sdk.InitData initData = new ai.haptik.android.sdk.InitData.Builder(MahindraFinanceApplication.getInstance())
 //                                .baseUrl("https://ziman.haptikapi.com/")
 //                                .notificationSound(R.raw.beep)
 ////                .verifyUserService(new UserVerificationService())
 ////                                .imageLoadingService(GlideApiFactory.getGlideApi())
 //                                .build();
 //                        HaptikLib.init(initData);
 //                        HaptikNotificationManager.handleNotification(this, data);
 //                    }
 //                });
 //            } else {
 //                HaptikNotificationManager.handleNotification(this, data);
 //            }
 //        }

         notificationManager =
                 (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

         //Setting up Notification channels for android O and above
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
             setupChannels();
         }

         Log.d(TAG, "Message " + remoteMessage.getNotification());
         Log.d(TAG, "Message " + remoteMessage.getData());

         // Check if message contains a notification payload.
         if (remoteMessage.getNotification() != null) {
             Log.v(TAG, "Notification Title: " + remoteMessage.getNotification().getTitle());
             Log.v(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
         }

         if (remoteMessage.getData().size() > 0) {
             Log.v(TAG, "Message data payload: " + remoteMessage.getData());
         }


         Intent notifyIntent = new Intent(this, GetTrackingActivity.class);
         // Create the TaskStackBuilder and add the intent, which inflates the back stack
         TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
         stackBuilder.addNextIntentWithParentStack(notifyIntent);
         notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
         if (remoteMessage.getNotification() != null) {
             notifyIntent.putExtra("notificationBodyString", remoteMessage.getNotification().getBody());
         } else {
             String body = remoteMessage.getData().get("body");
             try {
                 JSONObject jsonObject = new JSONObject(body);
                 notifyIntent.putExtra("notificationBodyString", jsonObject.get("body").toString());
             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }

         PendingIntent notifyPendingIntent = PendingIntent.getActivity(this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
         Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
         NotificationCompat.Builder notificationBuilder = null;

         if (remoteMessage.getData().size() > 0) {
             String body = remoteMessage.getData().get("body");
             try {
                 JSONObject jsonObject = new JSONObject(remoteMessage.getData());

                 notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                         .setSmallIcon(R.mipmap.ic_launcher)  //a resource for your custom small icon
                         .setContentTitle(jsonObject.get("title").toString()) //the "title" value you sent in your Data
                         .setContentText(jsonObject.get("body").toString()) //the "title" value you sent in your Data
 //                .setContentText(remoteMessage.getData().get("message")) //ditto
                         .setAutoCancel(true)  //dismisses the notification on click
                         .setSound(defaultSoundUri)
                         .setContentIntent(notifyPendingIntent);

                 Intent intent = new Intent("speedExceeded");
                 intent.putExtra("notificationBodyString", jsonObject.get("body").toString());
                 LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
             } catch (JSONException e) {
                 e.printStackTrace();
             }


         } else {
             notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                     .setContentTitle(remoteMessage.getNotification().getTitle()) //the "title" value you sent in your notification
                     .setContentText(remoteMessage.getNotification().getBody()) //ditto
                     .setAutoCancel(true)  //dismisses the notification on click
                     .setSound(defaultSoundUri)
                     .setSmallIcon(R.mipmap.ic_launcher)
                     .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                     .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                     .setColor(Color.parseColor("#FFD600"))
                     .setChannelId("Ziman")
                     .setLights(Color.BLUE, 500, 500)
                     .setVibrate(new long[]{100, 200, 300, 400})
                     .setPriority(NotificationCompat.PRIORITY_HIGH)
                     .setContentIntent(notifyPendingIntent);

             Intent intent = new Intent("speedExceeded");
             intent.putExtra("notificationBodyString", remoteMessage.getNotification().getBody());
             LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
         }


         NotificationManager notificationManager =
                 (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

         notificationManager.notify(111, notificationBuilder.build());
     }
 */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "Admin Channel Name";
        String adminChannelDescription = "Admin Channel Description";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        adminChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        System.out.println("HERE 123");

        Log.v(TAG, "Message N " + new Gson().toJson(remoteMessage.getNotification()));
        Log.v(TAG, "Message D " + remoteMessage.getData());
//        if (remoteMessage.getNotification() != null) {
//            sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
//
//            Intent intent = new Intent("speedExceeded");
//            intent.putExtra("notificationBodyString", remoteMessage.getNotification().getBody());
//            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//        } else {
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());
            sendNotification(jsonObject.get("title").toString(), jsonObject.get("body").toString());

            Intent intent = new Intent("speedExceeded");
            intent.putExtra("notificationBodyString", jsonObject.get("body").toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        }
    }

    private void sendNotification(String title, String messageBody) {
        System.out.println("HERE");
        Intent intent = new Intent(this, GetTrackingActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//Add Any key-value to pass extras to intent
//        intent.putExtra("push_notification", "yes");
        intent.putExtra("notificationBodyString", messageBody);
        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
//            mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }
//For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        mBuilder.setContentTitle(title)
                .setContentText(messageBody)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setOngoing(true)
//                .setSound(defaultSoundUri)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotifyManager.notify(1111, mBuilder.build());
//        count++;
    }

    /**
     * Create and push the notification
     */
    public void createNotification(String title, String message) {
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(this, GetTrackingActivity.class);
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("notificationBodyString", message);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(count /* Request Code */, mBuilder.build());
        count++;
    }


}
