/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.say.mahindrafinance.tracking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.say.mahindrafinance.BuildConfig;
import com.say.mahindrafinance.R;
import com.say.mahindrafinance.activities.MainActivity;
import com.say.mahindrafinance.base.MahindraFinanceApplication;
import com.say.mahindrafinance.helperClasses.AppConstants;
import com.say.mahindrafinance.helperClasses.SessionManager;
import com.say.mahindrafinance.interfaces.AppDataUrls;
import com.say.mahindrafinance.pojo.get_tracking.GetTrackingResponse;
import com.say.mahindrafinance.pojo.get_tracking.TriggerList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class PreTriggerTrackerService extends Service implements LocationListener {

    public static final String STATUS_INTENT = "status";
    private static final String TAG = PreTriggerTrackerService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 1;
    private static final int FOREGROUND_SERVICE_ID = 1;
    private static final int CONFIG_CACHE_EXPIRY = 600;  // 10 minutes.
    String trackingId = "";
    //    String shareTimeMin = "";
//    String startTime = "";
//    String currentTime = "";
    SessionManager sessionManager;
    Handler handle = new Handler();
    Timer t = new Timer();
    private GoogleApiClient mGoogleApiClient;
    private DatabaseReference mFirebaseTransportRef;
    private DatabaseReference mFirebaseTriggerTransportRef;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private LinkedList<Map<String, Object>> mTransportLocationStatuses = new LinkedList<>();
    private LinkedList<Map<String, Object>> mTransportTriggerStatuses = new LinkedList<>();
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private PowerManager.WakeLock mWakelock;
    private SharedPreferences mPrefs;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1: {
                    stopSelf();
                }
                break;
            }
        }
    };
    private Timer timer;
    private GoogleApiClient.ConnectionCallbacks mLocationRequestCallback = new GoogleApiClient
            .ConnectionCallbacks() {

        @SuppressLint("InvalidWakeLockTag")
        @Override
        public void onConnected(Bundle bundle) {
            LocationRequest request = new LocationRequest();
//            request.setSmallestDisplacement(3.0f);
            request.setInterval(mFirebaseRemoteConfig.getLong("LOCATION_REQUEST_INTERVAL"));
            request.setFastestInterval(mFirebaseRemoteConfig.getLong
                    ("LOCATION_REQUEST_INTERVAL_FASTEST"));
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (mGoogleApiClient.isConnected()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        return;
                    }
                }
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        request, PreTriggerTrackerService.this);
//            setStatusMessage(R.string.tracking);

                // Hold a partial wake lock to keep CPU awake when the we're tracking location.
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                if(Build.VERSION.SDK_INT >= 23) {
                    mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
                } else {
                    mWakelock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakelockTag");
                }

//                mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
                mWakelock.acquire();
            }
        }

        @Override
        public void onConnectionSuspended(int reason) {
            // TODO: Handle gracefully
        }
    };

    public PreTriggerTrackerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //nTaskRemoved(intent);

        sessionManager = new SessionManager(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        trackingId = mPrefs.getString("pre_trigger_transport_id", "");
        String email = mPrefs.getString("email", "");
        String password = mPrefs.getString("password", "");
//        shareTimeMin = mPrefs.getString("shareTimeMin", "1");
//        startTime = mPrefs.getString("startTime", "");
//        System.out.println("startTime = " + startTime);

        System.out.println("email: " + email + " password: " + password);


        if (!email.equals("") && !password.equals("")) {
            authenticate(email, password);
        }

        getTrackingApiCall();

        return Service.START_STICKY;
    }

    private void getTrackingApiCall() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("getTracking Resp Ser=", response);

                        System.out.println("GetTracking Response: " + response);
                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TriggerList> triggerList = getTrackingResponse.triggerList;
                            if (!triggerList.isEmpty()) {
                                TriggerList trackeeList1 = triggerList.get(triggerList.size() - 1);
                                if (trackeeList1 != null) {
                                    String startTime = trackeeList1.startTime;
                                    String currentTime = trackeeList1.currentTime;
                                    if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(currentTime)) {

                                        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        try {
                                            Date startTimeDate = input.parse(startTime);
                                            Date currentDate = input.parse(currentTime);
//                                            System.out.println("currentTime = " + currentDate);

                                            long diff = currentDate.getTime() - startTimeDate.getTime();
                                            long seconds = diff / 1000;
                                            long minutes = seconds / 60;

                                            String minDifference = String.valueOf(minutes);
                                            if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(trackeeList1.shareTime)) {
                                                int sTime = Integer.parseInt(trackeeList1.shareTime);
                                                int diffTime = Integer.parseInt(minDifference);
                                                if (diffTime > sTime) {
                                                    stopSelf();
                                                    // stop alarm also
                                                    Intent myAlarmCancel = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
                                                    //PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarmCancel, PendingIntent.FLAG_CANCEL_CURRENT);
                                                    PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarmCancel, PendingIntent.FLAG_IMMUTABLE);
                                                    AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                    alarms.cancel(recurringAlarm);

                                                } else {

                                                    int dif = (sTime - diffTime);
                                                    long timerShareTimeTiming = dif * 60 * 1000;

                                                    System.out.println("timerShareTimeTiming: " + timerShareTimeTiming);
                                                    t.schedule(new TimerTask() {
                                                        @Override
                                                        public void run() {
                                                            handle.post(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    stopSelf();
//                                                                    System.out.println("HERE 1");
//                                                                    updateTrackingApiCall();
                                                                    // stop alarm also
                                                                    Intent myAlarmCancel = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
                                                                    PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarmCancel, PendingIntent.FLAG_IMMUTABLE);
                                                                    //PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarmCancel, PendingIntent.FLAG_CANCEL_CURRENT);
                                                                    AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                                    alarms.cancel(recurringAlarm);

                                                                }
                                                            });
                                                        }
                                                    }, timerShareTimeTiming);
                                                }
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

//                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        System.out.println("onTaskRemove");
        Intent broadcastIntent = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
        broadcastIntent.setAction("prerestartservice");
        sendBroadcast(broadcastIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        System.out.println("Service destroyed");
        // Stop receiving location updates.
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    PreTriggerTrackerService.this);
        }
        // Release the wakelock
        if (mWakelock != null) {
            mWakelock.release();
        }
    }

    private void authenticate(String email, String password) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            fetchRemoteConfig();
                            mTransportLocationStatuses.clear();
                            mTransportTriggerStatuses.clear();
                            loadPreviousStatuses();
                        } else {
                            stopSelf();
                        }
                    }
                });
    }

    private void fetchRemoteConfig() {
        long cacheExpiration = CONFIG_CACHE_EXPIRY;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
//                        Log.i(TAG, "Remote config fetched");
                        mFirebaseRemoteConfig.activateFetched();
                    }
                });
    }

    /**
     * Loads previously stored statuses from Firebase, and once retrieved,
     * start location tracking.
     */
    private void loadPreviousStatuses() {
        FirebaseAnalytics.getInstance(this).setUserProperty("transportID", trackingId);

        String pathLocation = getString(R.string.firebase_path) + trackingId + "/location";
        System.out.println("pathLocation = " + pathLocation);
        mFirebaseTransportRef = FirebaseDatabase.getInstance().getReference(pathLocation);
        mFirebaseTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    for (DataSnapshot transportStatus : snapshot.getChildren()) {
                        mTransportLocationStatuses.add(Integer.parseInt(transportStatus.getKey()),
                                (Map<String, Object>) transportStatus.getValue());
                    }
                }
                startLocationTracking();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });


        String pathData = getString(R.string.firebase_path) + trackingId + "/everyMinute";
        mFirebaseTriggerTransportRef = FirebaseDatabase.getInstance().getReference(pathData);
        mFirebaseTriggerTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    for (DataSnapshot transportStatus : snapshot.getChildren()) {
                        mTransportTriggerStatuses.add(Integer.parseInt(transportStatus.getKey()),
                                (Map<String, Object>) transportStatus.getValue());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });

    }

    /**
     * Starts location tracking by creating a Google API client, and
     * requesting location updates.
     */
    private void startLocationTracking() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(mLocationRequestCallback)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private float getBatteryLevel() {
        Intent batteryStatus = registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int batteryLevel = -1;
        int batteryScale = 1;
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel);
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale);
        }
        return batteryLevel / (float) batteryScale * 100;
    }

    /**
     * Pushes a new status to Firebase when location changes.
     */
    @Override
    public void onLocationChanged(final Location location) {

        fetchRemoteConfig();
        long currentTime = new Date().getTime();
        Map<String, Object> transportStatus = new HashMap<>();
        transportStatus.put("lat", location.getLatitude());
        transportStatus.put("lng", location.getLongitude());
        transportStatus.put("time", currentTime);
        transportStatus.put("power", getBatteryLevel());

//        if (locationIsAtStatus(location, 1) && locationIsAtStatus(location, 0)) {
//            // If the most recent two statuses are approximately at the same
//            // location as the new current location, rather than adding the new
//            // location, we update the latest status with the current. Two statuses
//            // are kept when the locations are the same, the earlier representing
//            // the time the location was arrived at, and the latest representing the
//            // current time.
//
//            mTransportLocationStatuses.set(0, transportStatus);
//            // Only need to update 0th status, so we can save bandwidth.
//            mFirebaseTransportRef.child("0").setValue(transportStatus);
//
//            long timerShareTimeTiming = 10 * 60 * 1000;
//            Timer t = new Timer();
//            t.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    handle.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            System.out.println("10 min call pre trigger notification.");
//                            callPreTriggerNotification(location.getLatitude(), location.getLongitude());
//                        }
//                    });
//                }
//            }, timerShareTimeTiming);
//        } else {
//            // Maintain a fixed number of previous statuses.
//            while (mTransportLocationStatuses.size() >= mFirebaseRemoteConfig.getLong("MAX_STATUSES")) {
//                mTransportLocationStatuses.removeLast();
//            }
        mTransportLocationStatuses.addFirst(transportStatus);
        // We push the entire list at once since each key/index changes, to
        // minimize network requests.
        mFirebaseTransportRef.setValue(mTransportLocationStatuses);

        if (mTransportTriggerStatuses.isEmpty()) {
            mTransportTriggerStatuses.addFirst(transportStatus);
            mFirebaseTriggerTransportRef.setValue(mTransportTriggerStatuses);
        } else {
            Map<String, Object> first = mTransportTriggerStatuses.get(0);
            if (first != null) {
                long first_time = (long) first.get("time");
                try {
                    long diff = currentTime - first_time;
                    long seconds = diff / 1000;

                    String minDifference = String.valueOf(seconds);
                    System.out.println("secDifference = " + minDifference);
                    if (!TextUtils.isEmpty(minDifference) && Integer.parseInt(minDifference) >= 60){
                        mTransportTriggerStatuses.addFirst(transportStatus);
                        mFirebaseTriggerTransportRef.setValue(mTransportTriggerStatuses);
                        System.out.println("mTransportTriggerStatuses = " + mTransportTriggerStatuses.size());

                        if (!mTransportTriggerStatuses.isEmpty() && mTransportTriggerStatuses.size() > 10){
                            System.out.println("Location 0 = " + mTransportTriggerStatuses.get(0));
                            System.out.println("Location 10 = " + mTransportTriggerStatuses.get(10));
                            Map<String, Object> last = mTransportTriggerStatuses.get(10);
                            if (last != null) {
                                long last_time = (long) last.get("time");

//                                String firstDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(first_time));
//                                String lastDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(last_time));
//
//                                System.out.println("snapshot FIRST = " + firstDateString);
//                                System.out.println("snapshot Last = " + lastDateString);
                                try {
//                                    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                    Date startTimeDate = input.parse(lastDateString);
//                                    Date currentDate = input.parse(firstDateString);

//                                    long diff1 = currentDate.getTime() - startTimeDate.getTime();
                                    long diff1 = currentTime - last_time;
                                    long seconds1 = diff1 / 1000;
                                    long minutes = seconds1 / 60;

                                    String minDifference1 = String.valueOf(minutes);
                                    System.out.println("minDifference = " + minDifference1);
                                    if (!TextUtils.isEmpty(minDifference1) && Integer.parseInt(minDifference1) >= 9){
                                        Location locationZero = new Location("");
                                        locationZero.setLatitude((double) first.get("lat"));
                                        locationZero.setLongitude((double) first.get("lng"));

                                        Location locationLast = new Location("");
                                        locationLast.setLatitude((double) last.get("lat"));
                                        locationLast.setLongitude((double) last.get("lng"));

                                        float distance = locationLast.distanceTo(locationZero);
                                        System.out.println("Distance 0-10 = " + distance);

                                        if (distance < 50.0f){
//                                if (!isCallPreTriggerApiCall) {
                                            callPreTriggerNotification((double) first.get("lat"), (double) first.get("lng"));
//                                }
                                        }

                                    }

                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


//        }
    }

    private void callPreTriggerNotification(final double latitude, final double longitude) {
        System.out.println("PreTrigger URL = " + AppDataUrls.postPreTriggerNotification());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postPreTriggerNotification(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("PreTrigger Response = " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status) && status.equals("Success")){

                                //Toast.makeText(getApplicationContext(), "Notification pretrigger", Toast.LENGTH_SHORT).show();
                                //stop alarm also
//                               Intent myAlarm = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
//                               PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
//                                AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//                                alarms.cancel(recurringAlarm);

//                                updateTrackingApiCall();
//                                stopSelf();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body;
                        //get status code here
                        String statusCode = String.valueOf(error.networkResponse.statusCode);
                        //get response body and parse with appropriate encoding
                        if(error.networkResponse.data!=null) {
                            try {
                                body = new String(error.networkResponse.data,"UTF-8");
                                System.out.println("PreTriggerTrackerService Error: " + body);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MahindraFinanceApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private boolean locationIsAtStatus(Location location, int statusIndex) {
        if (mTransportLocationStatuses.size() <= statusIndex) {
            return false;
        }
        Map<String, Object> status = mTransportLocationStatuses.get(statusIndex);
        Location locationForStatus = new Location("");
        locationForStatus.setLatitude((double) status.get("lat"));
        locationForStatus.setLongitude((double) status.get("lng"));
        float distance = location.distanceTo(locationForStatus);
        Log.d(TAG, String.format("Distance from status %s is %sm", statusIndex, distance));
        return distance < mFirebaseRemoteConfig.getLong("LOCATION_MIN_DISTANCE_CHANGED");
    }


    private void buildNotification() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_ziman_logo)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle(getString(R.string.app_name))
                .setOngoing(true)
                .setContentIntent(resultPendingIntent);
        startForeground(FOREGROUND_SERVICE_ID, mNotificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.say.mahindrafinance";
        String channelName = "Ziman Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.createNotificationChannel(chan);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        mNotificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = mNotificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_ziman_logo)
                .setAutoCancel(true)
                .setOngoing(true)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(resultPendingIntent)
                .build();
        startForeground(2, notification);
    }
}
