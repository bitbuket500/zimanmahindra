package com.say.mahindrafinance.tracking;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class PreTriggerBroadcastReceiver extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("PreTriggerBroadcastReceiver Broadcast Listened. Service tried to stop 1");
//        Toast.makeText(context, "Service restarted", Toast.LENGTH_SHORT).show();
        this.context = context;

        PreTriggerTrackerService mYourService = new PreTriggerTrackerService();
        Intent mServiceIntent1 = new Intent(context, mYourService.getClass());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            System.out.println("PreTriggerBroadcastReceiver 1");
            if (!isMyServiceRunning(mYourService.getClass())) {
                System.out.println("PreTriggerBroadcastReceiver 2");
                context.startForegroundService(mServiceIntent1);
            }
        } else {
            System.out.println("PreTriggerBroadcastReceiver 3");
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startService(mServiceIntent1);
                System.out.println("PreTriggerBroadcastReceiver 4");
            }
        }

//        MyLocationService myLocationService = new MyLocationService();
//        Intent mServiceIntent = new Intent(context, myLocationService.getClass());
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (!isMyServiceRunning(myLocationService.getClass())) {
//                context.startForegroundService(mServiceIntent);
//            }
//        } else {
//            if (!isMyServiceRunning(myLocationService.getClass())) {
//                context.startService(mServiceIntent);
//            }
//        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("PreTriggerBroadStatus", "Running");
//                Log.i("Service status", "Running");
                return true;
            }
        }
//        Log.i("Service status", "Not running");
        return false;
    }


}