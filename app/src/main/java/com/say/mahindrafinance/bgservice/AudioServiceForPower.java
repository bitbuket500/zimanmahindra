package com.say.mahindrafinance.bgservice;

import static android.view.View.GONE;
import static com.say.mahindrafinance.bgservice.CameraService.getRetrofitInterface_NoHeader;

import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.say.mahindrafinance.R;
import com.say.mahindrafinance.activities.MainActivity;
import com.say.mahindrafinance.helperClasses.AppConstants;
import com.say.mahindrafinance.helperClasses.SessionManager;
import com.say.mahindrafinance.pojo.MultimediaResponse;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The AudioService is a background service. It captures the audio via microphone for a given amount of time
 * and uploads it to the server.
 *
 * @author QVS Pvt. Ltd
 */
public class AudioServiceForPower extends Service {

    private static final String INFO_TAG = "PANIC_AS_S";
    private static String mFileName = "";
    private MediaRecorder mRecorder = null;
  //  private String sPanicId = Config.DEFAULT_PANIC_ID;
    private Timer timer;
    private Handler handler;
    private SharedPreferences defaultPrefs;
    private SessionManager sessionManager;
    private Context context;
    private Integer oldStreamVolume;
    private AudioManager audioMgr;
    private String extension;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * Not used, but is called when the service is created.
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void muteShutterSound() {
        //trying to mute the camera shutter sound for 2 seconds
        try {
            audioMgr = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            oldStreamVolume = audioMgr.getStreamMaxVolume(AudioManager.STREAM_RING);
            //oldStreamVolume = audioMgr.getStreamVolume(AudioManager.STREAM_RING);
            System.out.println("sound value: " + oldStreamVolume);
            enableSound();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Called when the service is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        //Audio Service is the last to run when panic is triggered, and hence turn the panic on key to false
//		new Functions().setSharedPreferencesBoolean(this, Config.SHARED_PREF_KEY_IS_PANIC_ON, false);
    }

    /**
     * Called when the service is started. It will start the recorder to record the audio
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            handler = new Handler();
            defaultPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            sessionManager = new SessionManager(this);
            context = this;
            Bundle bundle = intent.getExtras();
            if (bundle != null && !bundle.isEmpty()) {
               /* try {
                    if (bundle.get(Config.PING_PANIC_ID) != null &&
                            !bundle.get(Config.PING_PANIC_ID).equals(Config.DEFAULT_PANIC_ID)) {
                        sPanicId = bundle.get(Config.PING_PANIC_ID).toString();
                        System.out.println("AudioService sPanicId: " + sPanicId);
                    }
                } catch (Exception e) {
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Audio service start command panic id error. ".concat(e.getMessage()));

                    e.printStackTrace();
                }*/
            }

            mRecorder = new MediaRecorder();
            onRecord(true);
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    onRecord(false);
                }
            }, Integer.parseInt(defaultPrefs.getString("audio_timeout", "10")) * 1000);
        } catch (Exception e) {
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Audio service start command error. ".concat(e.getMessage()));

            e.printStackTrace();
            onRecord(false);
        }
        return Service.START_STICKY;
    }

    /**
     * It will start or stop the recording depending on the given input value
     *
     * @param start boolean value to check whether to start or stop the recording
     */
    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    /**
     * This function sets the recording parameters including the source, format, path where the file will be saved.
     * It then starts the recording.
     */
    private void startRecording() {
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        //mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        //mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());

        File pictureFileDir = new File(context.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
            pictureFileDir.mkdir();
        }

        mFileName = pictureFileDir + dateFormat.format(new Date()) + "_audio.m4a";
        //mFileName = Config.PANIC_FOLDER_NAME + dateFormat.format(new Date()) + "_audio.m4a";
        mRecorder.setOutputFile(mFileName);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Microphone is in use by other application in audio service error. ".concat(e.getMessage()));

            e.printStackTrace();
            Log.e(INFO_TAG, "prepare() failed");
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Toast.makeText(AudioServiceForPower.this, "Microphone is in use by other application.", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            stopSelf();
        }
        mRecorder.start();
    }

    /**
     * This function stops the recording and saves the file to the sdcard.
     * It releases the recorder for other application's use. It then starts the background task to upload the
     * saved audio file and then closes the service itself.
     */
    private void stopRecording() {
        if (mRecorder != null) {
            Log.d(INFO_TAG, "mRecorder closed");
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                //new code for checking file extension
                int index = mFileName.lastIndexOf('.');
                if(index > 0) {
                    extension = mFileName.substring(index + 1);
                    System.out.println("File extension is " + extension);
                }

                if(extension.equals("m4a")) {
                    File file = new File(mFileName);
                    System.out.println("UploadMediaTask - Audio " + mFileName);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(AudioServiceForPower.this);
                                if (mPrefs.getBoolean("toast", false)) {
                                    Toast.makeText(AudioServiceForPower.this, "New Audio saved:" + mFileName,
                                            Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    System.out.println("UploadMediaTask - Audio");
                    uploadPictureFile(file, "2");
                }
                //end code

                /*File file = new File(mFileName);
                System.out.println("UploadMediaTask - Audio " + mFileName);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(AudioService.this);
                            if (mPrefs.getBoolean("toast", false)) {
                                Toast.makeText(AudioService.this, "New Audio saved:" + mFileName,
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                System.out.println("UploadMediaTask - Audio");
                uploadPictureFile(file, "2");*/

                //stopSelf();
            }
        } else {
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Audio service - mRecorder is null error. ");
            Log.d(INFO_TAG, "mRecorder is null");
        }
    }

    private void enableSound() {
        if (audioMgr != null && oldStreamVolume != null) {
            audioMgr.setStreamVolume(AudioManager.STREAM_RING, oldStreamVolume, 0);
        }
    }

    private void uploadPictureFile(File pictureFile, String con_type) {
        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserId());
        RequestBody panic_id = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getPanicId());
        RequestBody user_lat = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLatitude());
        RequestBody user_long = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLongitude());
        RequestBody auth_key = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getAuthKey());
        RequestBody app_key = RequestBody.create(MediaType.parse("multipart/form-data"), AppConstants.APP_SECURITY_KEY_VALUE);
        RequestBody content_type = RequestBody.create(MediaType.parse("multipart/form-data"), con_type);
        RequestBody module_type = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody notesString = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        final RequestBody requestBody = RequestBody.create(MediaType.parse("audio/*"), pictureFile);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image[]", pictureFile.getName(), requestBody);
        parts.add(fileToUpload);
        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader().saveMultiMediaImage(user_id,panic_id, user_lat, user_long, auth_key, app_key, content_type, module_type,notesString,type, parts)
                        .enqueue(new Callback<MultimediaResponse>() {
                            @Override
                            public void onResponse(Call<MultimediaResponse> call, Response<MultimediaResponse> response) {
                                System.out.println("Media Response Audio = " + response);

                                muteShutterSound();
//                                assert response.body() != null;
//                                if (response.body().status.equals(AppConstants.SUCCESS)) {
//                                    System.out.println(response.body().result);
//                                }

                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        System.out.println("Remove / Stop Service ");
                                        Intent intent1 = new Intent(context, CameraService.class);
                                        stopService(intent1);
                                        Intent intent2 = new Intent(context, AudioServiceForPower.class);
                                        stopService(intent2);

                                        if (MainActivity.heartbeat != null) {
                                            MainActivity.textViewSeconds.setText("");
                                            MainActivity.textViewSeconds.setVisibility(GONE);
                                            MainActivity.heartbeat.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_panic_button_yellow));
                                            MainActivity.heartbeat.start();
                                        }

                                    }
                                }, 1000);
                            }

                            @Override
                            public void onFailure(Call<MultimediaResponse> call, Throwable t) {

                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                MainActivity.logStatusToStorage(sessionManager.getUserId(), "Audio service file not uploading network error. ".concat(e.getMessage()));
            }
        }
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

}
