package com.say.mahindrafinance.interfaces;

public class AppDataUrls {

    //public static final String BASE_URL = "https://schooldex.in/backend/api/";
    public static final String BASE_URL = "https://mahindrafinance.zicomsaas.com/backend/index.php/api/";

    public AppDataUrls() {}

    public static String postLogin() {
        return BASE_URL + "login";
    }

    public static String postSentOTP() {
        return BASE_URL + "sendOTP";
    }

    public static String postVerifyOTP() {
        return BASE_URL + "verifyOTP";
    }

    public static String postMedia() {
        return BASE_URL + "saveMultimedia";
    }

    public static String updateUserDetails() {
        return BASE_URL + "updateUserDetails";
    }

    public static String getEmergencyContacts() {
        return BASE_URL + "getEmergencyContacts";
    }

    public static String postSetTracking() { return BASE_URL + "setTracking"; }

    public static String postUpdateTracking() {
        return BASE_URL + "updateTracking";
    }

    public static String postGetTracking() {
        return BASE_URL + "getTracking";
    }

    public static String postCreatePanicRequest() {
        return BASE_URL + "createPanicRequest";
    }

    public static String postAboutUsNPolicty() { return BASE_URL + "pages"; }

    public static String postRaiseAQuery() { return BASE_URL + "raiseQuery"; }

    public static String postPreTriggerNotification() { return BASE_URL + "preTriggerNotification"; }

    public static String getNotifications() { return BASE_URL + "getNotifications"; }

    public static String getQueryType() { return BASE_URL + "getQueryType"; }
}
